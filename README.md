# Mthdspool

Lists Ruby objects methods from `ObjectSpace` and later on filters them with `#grep`.

## <a name="Installation"></a>Installation:

With RubyGems, simply open a terminal and type:

    $ {sudo} gem install mthdspool

Local installation:

[Download](http://bitbucket.org/jpablobr/mthdspool/download) the tarball package and:

    $ tar -xvzf mthdspool-{version}.tgz

or

    $ git clone https://bitbucket.com/jpablobr/mthdspool.git

then 

    $ cd mthdspool-{version}
    $ {sudo} rake install

## Usage

    Usage: mthdspool [-v] [-h] command [<args>]

        -h, --help                       Print this help.
        -v, --version                    Print version.
        -l, --lib [library]              Library path
        -o, --object [object]            Object to inspect
        -i, --instance [intance]         Instance to inspect
        -f, --filter [filter]            Object methods filter

    > signifies inherited method.
    < signifies object specific method.

    Examples:
    mthdspool -o String -l ~/code/app/lib/app -f methods
    mthdspool -o String -f methods
    mthdspool -i \$stderr -f methods

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## <a name="copyright"></a>Copyright
Copyright (c) 2012 Jose Pablo Barrantes. See [LICENSE][] for details.

[license]: https://bitbucket.org/jpablobr/mthdspool/blob/master/LICENSE
