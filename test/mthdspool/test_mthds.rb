# -*- encoding: utf-8 -*-
require 'test_helper'
require 'minitest/autorun'

module MthdsPool
  class CLITest < MiniTest::Unit::TestCase

    def test_instance_object_should_receive_valid_constants
      assert_raises(NameError) {
        Mthds.new({inst: KDFGJ})
      }
    end

    def test_print_object
      print = Mthds.new({obj: 'StringIO', filter: 'singleton_methods'})
      assert_output(">IM StringIO#singleton_methods\n") {
        print.objects_methods
      }
    end
  end
end
