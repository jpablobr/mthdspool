module MthdsPool
  class CLI
    require 'optparse'
    def parse_options argv
      options={}
      opts = OptionParser.new do |o|
        o.banner = "Usage: mthdspool [-v] [-h] command [<args>]"
        o.separator ""
        o.on("-h", "--help", "Print this help.") {
          $stderr.puts(opts)
        }
        o.on("-v", "--version", "Print version.") {
          return $stderr.puts(VERSION)
        }
        o.on("-l", "--library [library]", "Library path") { |l|
          options[:libs] = l
        }
        o.on("-r", "--require [require]", "Require path") { |r|
          options[:reqr] = r
        }
        o.on("-g", "--gemfile [gemfile]", "Gemfile") { |g|
          options[:gemfile] = g
        }
        o.on("-i", "--inspect [inspect]", "Instance to inspect") { |i|
          options[:nspct] = i
        }
        o.on("-f", "--filter [filter]", "Object methods filter") { |f|
          options[:filter] = f
        }
        o.separator ""
        o.separator ">  = inherited method."
        o.separator "<  = object specific method."
        o.separator "IM = instance method."
        o.separator "SM = Singleton method."
        o.separator ""
        o.separator "Examples:"
        o.separator "mthdspool -o String -l ~/code/app/lib/app -f methods"
        o.separator "mthdspool -o String -f methods"
        o.separator "mthdspool -o \\$stderr -f methods"
        o.separator "mthdspool -i CONST -r ~/code/app/lib/app/test/test_helper.rb"
      end
      opts.parse!(argv) rescue return $stderr.puts(opts)
      prnt = Mthds.new(options)
      case
      when options.has_key?(:obj);  prnt.objects_methods
      when options.has_key?(:nspct); prnt.inspect_instance
      end
    end
  end
end
