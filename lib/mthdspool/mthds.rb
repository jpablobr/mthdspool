module MthdsPool
  class Mthds
    attr_accessor :libs, :obj, :filter, :reqr, :nspct, :gemfile

    def initialize opts={}
      @obj     = opts[:obj]
      @libs    = opts[:libs]
      @reqr    = opts[:reqr]
      @filter  = opts[:filter]
      @gemfile = opts[:gemfile]
      setup
      begin
        @nspct   = eval("#{opts[:nspct]}")
      rescue NameError => e
        puts e.message
        exit 1
      end
    end

    def objects_methods
      get_objects.each { |o| print_methods(o) }
    end

    def inspect_instance
      puts @nspct.inspect
    end

    private

    def get_objects
      ObjectSpace.each_object(Class).select { |obj|
        obj.to_s.match(/#{@obj}/) ? obj.to_s.gsub(/#{@obj}/, '') : nil
      }
    end

    def print_methods obj
      if obj.respond_to?(:singleton_methods)
        print_singleton_methods(obj)
        print_all_singleton_methods(obj)
      end
      if obj.respond_to?(:instance_methods)
        print_instance_methods(obj)
        print_all_instance_methods(obj)
      end
    end

    def print_all_singleton_methods obj
      mthds = obj.singleton_methods - (obj.instance_methods - Object.new.methods)
      mthds.grep(/#{@filter}/).each { |m|
        puts ">SM " +  obj.name + "#" + m.to_s
      }
    end

    def print_all_instance_methods obj
      mthds = obj.instance_methods - (obj.instance_methods - Object.new.methods)
      mthds.grep(/#{@filter}/).each { |m|
        puts ">IM " + obj.name + "#" + m.to_s
      }
    end

    def print_singleton_methods obj
      (obj.singleton_methods - Object.new.methods).grep(/#{@filter}/).each { |m|
        puts "<SM " +  obj.name + "#" + m.to_s
      }
    end

    def print_instance_methods obj
      (obj.instance_methods - Object.new.methods).grep(/#{@filter}/).each { |m|
        puts "<IM " + obj.name + "#" + m.to_s
      }
    end

    def setup
      bundler_setup   unless @gemfile.nil?
      load_path_setup unless @libs.nil?
      requires_setup  unless @reqr.nil?
    end

    def bundler_setup
      ENV['BUNDLE_GEMFILE'] = @gemfile
      require 'rubygems'
      require 'bundler/setup'
    end

    def load_path_setup
      libs.split(":").each { |l| $LOAD_PATH.unshift(l) }
    end

    def requires_setup
      reqr.split(":").each { |f| require f }
    end
  end
end
