# -*- encoding: utf-8 -*-
require File.expand_path('../lib/mthdspool/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["jpablobr"]
  gem.email         = ["xjpablobrx@gmail.com"]
  gem.description   = %q{Lists Ruby objects methods by searching them in ObjectSpace and filtering them with grep.}
  gem.summary       = %q{Lists Ruby objects methods.}
  gem.homepage      = ""

  gem.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  gem.files         = `git ls-files`.split("\n")
  gem.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  gem.name          = "mthdspool"
  gem.require_paths = ["lib"]
  gem.version       = MthdsPool::VERSION
end
